FROM debian as downloader
WORKDIR /dl
RUN apt update && apt install wget -y
RUN wget https://dlcdn.apache.org/pdfbox/2.0.32/pdfbox-app-2.0.32.jar --output-document pdfbox.jar

FROM amazoncorretto:21.0.5
COPY --from=downloader /dl/pdfbox.jar /root/pdfbox.jar
ENTRYPOINT [ "java", "-jar", "/root/pdfbox.jar"]
